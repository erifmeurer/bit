﻿using UnityEngine;
using System;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Runtime.InteropServices;


public class ConexãoSocket : MonoBehaviour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Conta
    {

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string Login;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string Senha;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string eMail;
    }




    Thread receiveThread;
    Thread receiveThread2;
    public static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    float timeCarreg = 2;
    int tentativa;
    public ControleDePacote controleDePacote;

    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        SetupServer();
    }
    private byte[] _recieveBuffer = new byte[8142];

    private void SetupServer()//inicia conexao
    {
        try
        {
            _clientSocket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 100));

        }
        catch (SocketException ex)
        {
            Debug.Log(ex.Message);
        }

        _clientSocket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);

    }

    private void ReceiveCallback(IAsyncResult AR)//recebe do servidor
    {
        int recieved = _clientSocket.EndReceive(AR);

        if (recieved <= 0)
            return;

        byte[] recData = new byte[recieved];
        Buffer.BlockCopy(_recieveBuffer, 0, recData, 0, recieved);
        controleDePacote.listaDePacotes.Add(recData);
        _clientSocket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
    }

    public static void SendData(byte[] data)//envia bytes
    {
        SocketAsyncEventArgs socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data, 0, data.Length);
        _clientSocket.SendAsync(socketAsyncData);
    }
  
    void OnApplicationQuit()
    {
        _clientSocket.Close();
    }
}
