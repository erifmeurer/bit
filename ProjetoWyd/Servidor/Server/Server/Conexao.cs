﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Conexao
    {
        public static List<Socket> listaConectados = new List<Socket>();
        public int idClients = 0;
        private static byte[] _buffer = new byte[1024];
        public static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private ControleDePacotes controle = new ControleDePacotes();

        public void SetupServer()
        {
            try
            {
                _serverSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 100));
                _serverSocket.Listen(5);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (SocketException e)
            {
            }

        }
        private void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);
            IPEndPoint newclient = (IPEndPoint)socket.RemoteEndPoint;
            try
            {
                socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                listaConectados.Add(socket);
            }
            catch
            {
                
            }
        }
        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            IPEndPoint newclient = (IPEndPoint)socket.RemoteEndPoint;
            try
            {

                int received = socket.EndReceive(AR);
                byte[] dataBuff = new byte[received];
                Array.Copy(_buffer, dataBuff, received);
                controle.controleDePacotes(dataBuff,socket);
                socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            }
            catch
            {
               
            }
        }
        public void SendBuff(byte[] buff, Socket socket)
        {
            socket.BeginSend(buff, 0, buff.Length, SocketFlags.None, new AsyncCallback(SendCall), socket);
            IPEndPoint newclient = (IPEndPoint)socket.RemoteEndPoint;
        }
        private void SendCall(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
            IPEndPoint newclient = (IPEndPoint)socket.RemoteEndPoint;
        }
    }
}
